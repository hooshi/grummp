# LaTeX2HTML 2018 (Released Feb 1, 2018)
# Associate images original text with physical files.


$key = q/-M_{L}+M_{S};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="$-M_{L}+M_{S}$">|; 

$key = q/01234;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img67.png"
 ALT="$01234$">|; 

$key = q/012T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img68.png"
 ALT="$012T$">|; 

$key = q/01TB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="$01TB$">|; 

$key = q/021B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img71.png"
 ALT="$021B$">|; 

$key = q/024T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img69.png"
 ALT="$024T$">|; 

$key = q/042B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img72.png"
 ALT="$042B$">|; 

$key = q/12TB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="$12TB$">|; 

$key = q/20^{textrm{o}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img81.png"
 ALT="$20^{\textrm{o}}$">|; 

$key = q/234T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img70.png"
 ALT="$234T$">|; 

$key = q/23TB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img64.png"
 ALT="$23TB$">|; 

$key = q/2N-4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="$2N-4$">|; 

$key = q/2^{D};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$2^{D}$">|; 

$key = q/30^{circ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="$30^{\circ}$">|; 

$key = q/324B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img73.png"
 ALT="$324B$">|; 

$key = q/34TB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img65.png"
 ALT="$34TB$">|; 

$key = q/3slash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$3/2$">|; 

$key = q/40TB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img66.png"
 ALT="$40TB$">|; 

$key = q/5^{circ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img78.png"
 ALT="$5^{\circ}$">|; 

$key = q/D;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img79.png"
 ALT="$D$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$G$">|; 

$key = q/LS(p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img48.png"
 ALT="$LS(p)$">|; 

$key = q/LS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="$LS$">|; 

$key = q/M_{L}=frac{2r}{sqrt{3}overline{LS}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="$M_{L}=\frac{2r}{\sqrt{3}\overline{LS}}$">|; 

$key = q/M_{S};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="$M_{S}$">|; 

$key = q/M_{S}=frac{sqrt{6}l_{min}}{4r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="45" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="$M_{S}=\frac{\sqrt{6}l_{\min}}{4r}$">|; 

$key = q/N+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img94.png"
 ALT="$N+1$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img59.png"
 ALT="$N$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$R$">|; 

$key = q/TB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="$TB$">|; 

$key = q/TV(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="$TV(\theta)$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img45.png"
 ALT="$a$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img46.png"
 ALT="$b$">|; 

$key = q/beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img82.png"
 ALT="$\beta$">|; 

$key = q/bigtriangleup056;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img86.png"
 ALT="$\bigtriangleup056$">|; 

$key = q/bigtriangleup061;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img85.png"
 ALT="$\bigtriangleup061$">|; 

$key = q/bigtriangleup156;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.png"
 ALT="$\bigtriangleup156$">|; 

$key = q/bigtriangleup345;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img91.png"
 ALT="$\bigtriangleup345$">|; 

$key = q/c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img47.png"
 ALT="$c$">|; 

$key = q/d;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$d$">|; 

$key = q/dslashG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="$d/G$">|; 

$key = q/frac{sqrt{2}}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="$\frac{\sqrt{2}}{2}$">|; 

$key = q/geq1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="$\geq1$">|; 

$key = q/includegraphics[width=0.45columnwidth]{picsslashsquare-circhole-bezier-R2G2};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="246" HEIGHT="206" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="\includegraphics[width=0.45\columnwidth]{pics/square-circhole-bezier-R2G2}">|; 

$key = q/includegraphics[width=0.45columnwidth]{picsslashsquare-circhole-bezier-bdry};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="246" HEIGHT="206" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="\includegraphics[width=0.45\columnwidth]{pics/square-circhole-bezier-bdry}">|; 

$key = q/includegraphics[width=0.4textwidth]{picsslashdiametral-lenses};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="221" HEIGHT="222" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="\includegraphics[width=0.4\textwidth]{pics/diametral-lenses}">|; 

$key = q/includegraphics[width=0.5textwidth]{picsslashbface2d};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="277" HEIGHT="178" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="\includegraphics[width=0.5\textwidth]{pics/bface2d}">|; 

$key = q/includegraphics[width=0.5textwidth]{picsslashcontract};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="277" HEIGHT="132" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img89.png"
 ALT="\includegraphics[width=0.5\textwidth]{pics/contract}">|; 

$key = q/includegraphics[width=0.5textwidth]{picsslashface-configs};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="277" HEIGHT="210" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img58.png"
 ALT="\includegraphics[width=0.5\textwidth]{pics/face-configs}">|; 

$key = q/includegraphics[width=0.5textwidth]{picsslashsurf-struct};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="274" HEIGHT="240" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img92.png"
 ALT="\includegraphics[width=0.5\textwidth]{pics/surf-struct}">|; 

$key = q/includegraphics[width=0.65textwidth]{picsslash5to6};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="361" HEIGHT="116" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img74.png"
 ALT="\includegraphics[width=0.65\textwidth]{pics/5to6}">|; 

$key = q/includegraphics[width=0.6textwidth]{picsslashdiagram-edgerecovery};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="332" HEIGHT="347" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="\includegraphics[width=0.6\textwidth]{pics/diagram-edgerecovery}">|; 

$key = q/includegraphics[width=0.6textwidth]{picsslashdiscspline};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="326" HEIGHT="113" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img43.png"
 ALT="\includegraphics[width=0.6\textwidth]{pics/discspline}">|; 

$key = q/includegraphics[width=0.6textwidth]{picsslashsmooth-section};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="327" HEIGHT="201" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img77.png"
 ALT="\includegraphics[width=0.6\textwidth]{pics/smooth-section}">|; 

$key = q/includegraphics[width=0.6textwidth]{picsslashte-schematic};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="331" HEIGHT="167" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img80.png"
 ALT="\includegraphics[width=0.6\textwidth]{pics/te-schematic}">|; 

$key = q/includegraphics[width=0.75textwidth]{picsslashface2d};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="414" HEIGHT="290" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="\includegraphics[width=0.75\textwidth]{pics/face2d}">|; 

$key = q/includegraphics[width=0.75textwidth]{picsslashface3d};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="411" HEIGHT="377" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="\includegraphics[width=0.75\textwidth]{pics/face3d}">|; 

$key = q/includegraphics[width=0.7textwidth]{picsslashconfigs};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="388" HEIGHT="181" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img76.png"
 ALT="\includegraphics[width=0.7\textwidth]{pics/configs}">|; 

$key = q/includegraphics[width=0.7textwidth]{picsslashframework};AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="223" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img40.png"
 ALT="\includegraphics[width=0.7\textwidth]{pics/framework}">|; 

$key = q/l!f!s(p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img49.png"
 ALT="$l\! f\! s(p)$">|; 

$key = q/l!f!s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="$l\! f\! s$">|; 

$key = q/l!f!s_{c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img50.png"
 ALT="$l\! f\! s_{c}$">|; 

$key = q/left(M_{L}>1right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="$\left(M_{L}&gt;1\right)$">|; 

$key = q/leq;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$\leq$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img93.png"
 ALT="$n$">|; 

$key = q/n_{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="$n_{x}$">|; 

$key = q/n_{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$n_{y}$">|; 

$key = q/n_{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$n_{z}$">|; 

$key = q/overline{01};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img87.png"
 ALT="$\overline{01}$">|; 

$key = q/overline{05};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img88.png"
 ALT="$\overline{05}$">|; 

$key = q/overline{06};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img84.png"
 ALT="$\overline{06}$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="$p$">|; 

$key = q/q_{i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img37.png"
 ALT="$q_{i}$">|; 

$key = q/rholeft(pright)=frac{1}{left|kappaleft(pright)right|};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img52.png"
 ALT="$\rho\left(p\right)=\frac{1}{\left\vert\kappa\left(p\right)\right\vert}$">|; 

$key = q/theta_{min};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$\theta_{\min}$">|; 

$key = q/theta_{min}=25.7^{circ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$\theta_{\min}=25.7^{\circ}$">|; 

$key = q/theta_{min}approx20.7^{circ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="86" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$\theta_{\min}\approx20.7^{\circ}$">|; 

$key = q/times;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img75.png"
 ALT="$\times$">|; 

$key = q/v1_{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$v1_{x}$">|; 

$key = q/v1_{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$v1_{y}$">|; 

$key = q/v1_{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$v1_{z}$">|; 

$key = q/v2_{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$v2_{x}$">|; 

$key = q/v2_{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$v2_{y}$">|; 

$key = q/v2_{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$v2_{z}$">|; 

$key = q/v3_{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$v3_{x}$">|; 

$key = q/v3_{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="$v3_{y}$">|; 

$key = q/v3_{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$v3_{z}$">|; 

$key = q/v_{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img83.png"
 ALT="$v_{A}$">|; 

$key = q/{displaymath}LSleft(pright)=minleft(frac{l!f!sleft(pright)}{R},min_{textrm{neigh_{i}right)+frac{1}{G}left|vec{q}_{i}-vec{p}right|right){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="341" HEIGHT="45" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="\begin{displaymath}
LS\left(p\right)=\min\left(\frac{l\! f\! s\left(p\right)}{R}...
...ht)+\frac{1}{G}\left\vert\vec{q}_{i}-\vec{p}\right\vert\right)
\end{displaymath}">|; 

$key = q/{displaymath}TV(theta)=intmiddthetamid{displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="39" BORDER="0"
 SRC="|."$dir".q|img42.png"
 ALT="\begin{displaymath}
TV(\theta)=\int\mid d\theta\mid
\end{displaymath}">|; 

$key = q/{displaymath}l!f!s_{c}(p)=minleft(rholeft(pright),,l!f!sleft(pright)right){displaymath};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="185" HEIGHT="29" BORDER="0"
 SRC="|."$dir".q|img51.png"
 ALT="\begin{displaymath}
l\! f\! s_{c}(p)=\min\left(\rho\left(p\right), l\! f\! s\left(p\right)\right)
\end{displaymath}">|; 

$key = q/{figure}{par{list{{}{{setlength{rightmargin}{leftmargin}{{setlength{listparindenlygon~r~1~r~3~4~0~2~8~6{par{polygon~r~3~r~4~4~0~3~9~6list{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="486" HEIGHT="778" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="\begin{figure}\par\begin{list}{}{\setlength{\rightmargin}{\leftmargin}\setl...
...r
polygon&nbsp;r&nbsp;1&nbsp;r&nbsp;3&nbsp;4&nbsp;0&nbsp;2&nbsp;8&nbsp;6
\par
polygon&nbsp;r&nbsp;3&nbsp;r&nbsp;4&nbsp;4&nbsp;0&nbsp;3&nbsp;9&nbsp;6\end{list}\end{figure}">|; 

$key = q/{figure}{par{list{{}{{setlength{rightmargin}{leftmargin}{{setlength{listparinden{{setlength{parsep}{0pt}{{normalfont{{ttfamily{}item[]~list{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="608" HEIGHT="700" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="\begin{figure}\par\begin{list}{}{\setlength{\rightmargin}{\leftmargin}\setl...
...\setlength{\parsep}{0pt}
\normalfont\ttfamily }\item[]&nbsp;\end{list}\end{figure}">|; 

$key = q/{figure}{par{list{{}{{setlength{rightmargin}{leftmargin}{{setlength{listparinden~verts{par{bdryfaces:~face~bc~verts{par{cells:~regionlist{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="265" HEIGHT="109" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="\begin{figure}\par\begin{list}{}{\setlength{\rightmargin}{\leftmargin}\setl...
...lls&nbsp;verts
\par
bdryfaces:&nbsp;face&nbsp;bc&nbsp;verts
\par
cells:&nbsp;region\end{list}\end{figure}">|; 

1;

