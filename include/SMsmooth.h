#ifndef SM_SMOOTH_H
#define SM_SMOOTH_H 1

/* define the structures associated with both geometric and octree
 representations of geometric entities... the void data type allows
 us to switch contexts 
 */

#include <assert.h>

#include "SM_config.h"
#include "SMqual_func.h"
#include "SMdefs.h"
#include "SMinternalFunction.h"
#include "OptMS.h"

#endif
